CFLAGS	+= -W -Wall -pedantic -o3 -g
CFLAGS	+= -I./header/ -I./libmy/header/

CC	= gcc
RM	= rm -f

LIB	= -lmy -L ./libmy/ -lm

NAME	= cpz

SRC	= \
	src/bit.c \
	src/cpz.c \
	src/crypt.c \
	src/gen.c \
	src/main.c \
	src/print.c \
	src/uncpz.c \
	src/huffman/cpz_huffman.c \
	src/huffman/table.c \
	src/huffman/uncpz_huffman.c \
	src/lz/cpz_lz.c \
	src/lz/find_field.c \
	src/lz/uncpz_lz.c

OBJ	= $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(CFLAGS) $(LIB) -o $(NAME)

lib:
	make -C libmy/
	make clean -C libmy/

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
