#include "my.h"
#include "struct.h"

void	crypt(t_string *str, const char *key)
{
  int	i;
  int	j;

  my_putstr("Crypting...\n");
  j = 0;
  i = -1;
  while (++i < str->len)
    {
      str->str[i] = (str->str[i] + key[j]) - j;
      j = key[j + 1] ? (j + 1) : 0;
      print_cent((i * 100) / str->len);
    }
  my_putchar('\n');
}

void	uncrypt(t_string *str, const char *key)
{
  int	i;
  int	j;

  my_putstr("Decrypting...\n");
  j = 0;
  i = -1;
  while (++i < str->len)
    {
      str->str[i] = (str->str[i] + j) - key[j];
      j = key[j + 1] ? (j + 1) : 0;
      print_cent((i * 100) / str->len);
    }
  my_putchar('\n');
}
