#include <unistd.h>

int		add_bit(char *file, const int value, const int last)
{
  static char	octet = 0;
  static int	bit = 0;
  static int	i = 0;

  octet <<= 1;
  octet |= value ? 1 : 0;
  bit = (bit + 1) % 8;
  if (!bit)
    file[i] = octet;
  else if (last)
    {
      octet <<= 8 - bit;
      file[i] = octet;
      octet = 0;
      bit = 0;
    }
  if (!bit || last)
    ++i;
  return (i);
}

int		get_nextbit(const char *file, const int last)
{
  static int	i = 0;
  static int	bit = 0;
  int		ret;

  ret = (file[i] >> (7 - bit)) & 1;
  bit = (bit + 1) % 8;
  if (!bit)
    ++i;
  if (last)
    {
      i = 0;
      bit = 0;
    }
  return (ret);
}

int	put_int(char *file, const int nb)
{
  int	i;

  i = 0;
  while (i < 4)
    {
      file[i] = (nb >> ((3 - i) << 3)) & 0xFF;
      ++i;
    }
  return (i);
}

int	get_int(const int fd)
{
  char	buffer[4];
  int	nb;
  int	i;

  if (read(fd, buffer, 4) == -1)
    return (-1);
  nb = 0;
  i = 0;
  while (i < 4)
    {
      nb += (int)(buffer[i]) << ((3 - i) << 3);
      ++i;
    }
  return (nb);
}
