#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "my.h"
#include "cpz.h"
#include "lz.h"
#include "huffman.h"
#include "crypt.h"
#include "struct.h"
#include "const.h"

static int	open_file(char *filename, char *new_filename, int fd[2])
{
  int		size;

  if ((fd[0] = my_open(filename, O_RDONLY)) == -1)
    return (-1);
  if (new_filename == NULL)
    {
      read(fd[0], &size, 1);
      if ((new_filename = malloc(sizeof(*new_filename) * size)) == NULL)
	return (-1);
      read(fd[0], new_filename, size);
      if ((fd[1] = my_open(new_filename, O_WRONLY | O_CREAT | O_TRUNC)) == -1)
	return (-1);
      free(new_filename);
    }
  else if ((fd[1] = my_open(new_filename, O_WRONLY | O_CREAT | O_TRUNC)) == -1)
    return (-1);
  return (0);
}

static void	update_buffer(t_string *dest_buffer, t_string *src_buffer)
{
  free(dest_buffer->str);
  dest_buffer->str = src_buffer->str;
  dest_buffer->len = src_buffer->len;
  free(src_buffer);
}

int	uncpz(char *filename, char *new_filename, const char *key)
{
  t_string	*buffer;
  t_string	*tmp;
  int		fd[2];
  char		type;
  int		size;

  if (open_file(filename, new_filename, fd) == -1)
    return (-1);
  if ((buffer = malloc(sizeof(*buffer))) == NULL)
    return (-1);

  my_putstr("Decompressing...\n");
  read(fd[0], &type, 1);
  while (type != '0')
    {
      size = get_int(fd[0]);
      if ((buffer->str = malloc(sizeof(*buffer->str) * size)) == NULL)
	return (-1);
      read(fd[0], &type, 1);
    }
  while ((buffer->len = read(fd[0], buffer->str, BUFFER_SIZE)) > 0)
    {
      my_putstr("Buffering...\n");
      if (key)
	uncrypt(buffer, key);
      if ((tmp = uncpz_lz(buffer)) == NULL)
	return (-1);
      update_buffer(buffer, tmp);
      if ((tmp = uncpz_huffman(buffer)) == NULL)
      	return (-1);
      update_buffer(buffer, tmp);
      write(fd[1], buffer->str, buffer->len);
    }
  my_fputchar(fd[1], STOP_CHAR);
  my_putchar('\n');

  free(buffer);
  close(fd[0]);
  close(fd[1]);
  return (0);
}
