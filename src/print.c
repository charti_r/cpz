#include "my.h"
#include "const.h"

void	print_help(void)
{
  my_printf("%s: help\n", PROG_NAME);
  my_printf("USAGE: %s [OPTION]... [FILE...]\n", PROG_NAME);
  my_putstr("OPTIONS:\n");
  my_putstr("  -c                    Compress following files\n");
  my_putstr("  -u                    Uncompress following files\n");
  my_putstr("  --help                Will print this help\n");
  my_putstr("  --name [filename]     Set new file name to [filename]\n");
  my_putstr("  --key [key]           Crypt following files with the key [key]\n");
  my_putstr("\n");
  my_putstr("Exit Status:\n");
  my_putstr("  0    It is OK\n");
  my_putstr("  -1   It failled\n");
}

void		print_cent(int cent)
{
  static int	tmp = 100;
  int		nb;
  int		i;

  if (tmp != cent)
    {
      tmp = (int)cent;
      my_putstr("\r[");
      nb = (20 * cent) / 100;
      i = -1;
      while (++i < nb)
	my_putchar('|');
      while (++i < 20)
	my_putchar(' ');
      my_printf("] %d%%", cent);
    }
}
