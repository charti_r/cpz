#include <stdlib.h>

#include "my.h"
#include "cpz.h"
#include "lz.h"
#include "struct.h"
#include "const.h"

static void	write_lz_hd(t_string *str, const char esc)
{
  str->str[0] = LZ_CHAR;
  put_int(&str->str[1], str->len - LZ_HD_SIZE);
  str->str[5] = esc;
}

static char	gen_esc_char(const t_string *str)
{
  char		esc;
  int		tmp;
  int		table[256];
  int		i;

  gen_table(str, table);
  tmp = table[0];
  esc = 0;
  i = 0;
  while (i < 0x7F)
    {
      if (tmp > table[i])
	{
	  tmp = table[i];
	  esc = i;
	}
      ++i;
    }
  return (esc);
}

t_string	*cpz_lz(t_string *str)
{
  t_string	*buffer;
  int		old_size;
  t_lz_field	field;
  char		esc;
  int		find;
  int		i;

  esc = gen_esc_char(str);
  if ((buffer = malloc(sizeof(*buffer))) == NULL)
    return (NULL);
  if ((buffer->str = malloc(sizeof(*buffer->str) * (str->len))) == NULL)
    return (NULL);
  buffer->len = LZ_HD_SIZE;
  old_size = 0;

  my_putstr("LZ...\n");
  i = -1;
  while (++i < str->len)
    {
      if ((find = find_field(str, i, &field)) == -1)
	return (NULL);
      else if (find)
	{
	  buffer->str[++buffer->len - 1] = esc;
	  buffer->str[++buffer->len - 1] = (field.dist >> 2) | 0x80;
	  buffer->str[++buffer->len - 1] = (field.dist << 6) | field.len;
	  i += field.len - 1;
	}
      else if (str->str[i] == esc)
	{
	  buffer->str[++buffer->len - 1] = esc;
	  buffer->str[++buffer->len - 1] = str->str[i];
	}
      else
	buffer->str[++buffer->len - 1] = str->str[i];
      print_cent((float)i / str->len * 100.0);
      if (buffer->len + 3 >= old_size + str->len)
	{
	  buffer->str = my_str_realloc(buffer->str, buffer->len, buffer->len + str->len);
	  if (buffer->str == NULL)
	    return (NULL);
	  old_size = buffer->len;
	}
    }
  my_putchar('\n');

  write_lz_hd(buffer, esc);
  return (buffer);
}
