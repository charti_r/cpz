#include "my.h"
#include "struct.h"
#include "const.h"

int	find_field(const t_string *str, const int index, t_lz_field *field)
{
  int	cmp;
  int	find;
  int	i;

  find = 0;
  field->len = 4;
  i = (index > LZ_LEN_MAX) ? (index - LZ_LEN_MAX) : 0;
  while (i < index)
    {
      if (field->len >= LZ_LEN_MAX || field->len + index >= str->len || i + field->len >= index)
	return (find);
      if (my_strncmp(&str->str[i], &str->str[index], field->len) == 0)
	{
	  find = 1;
	  field->dist = index - i;
	  cmp = 0;
	  while (!cmp && field->len < LZ_LEN_MAX && field->len + index < str->len && i + field->len < index)
	    {
	      cmp = my_strncmp(&str->str[i], &str->str[index], field->len + 1);
	      if (!cmp)
		++field->len;
	    }
	}
      ++i;
    }
  return (find);
}
