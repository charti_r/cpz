#include <stdlib.h>

#include "my.h"
#include "cpz.h"
#include "huffman.h"
#include "struct.h"
#include "const.h"

static void	write_huffman_hd(t_string *str, const char top[16])
{
  str->str[0] = HUFFMAN_CHAR;
  put_int(&str->str[1], str->len - HUFFMAN_HD_SIZE);
  my_strncpy(&str->str[5], top, 16);
}

static int	write_topchar(char *str, const int index, const int last)
{
  int		ret;
  int		i;

  add_bit(str, BTYPE_C, 0);
  i = 4;
  while (--i >= 0)
    ret = add_bit(str, (index >> i) & 1, i ? 0 : last);
  return (ret);
}

static int	write_stdchar(char *str, const char c, const int last)
{
  int		ret;
  int		i;

  add_bit(str, BTYPE_UC, 0);
  i = 8;
  while (--i >= 0)
    ret = add_bit(str, (c >> 1) & 1, i ? 0 : last);
  return (ret);
}

t_string	*cpz_huffman(t_string *str)
{
  t_string	*buffer;
  int		old_size;
  char		top[16];
  int		index;
  int		i;

  if ((buffer = malloc(sizeof(*buffer))) == NULL)
    return (NULL);
  if ((buffer->str = malloc(sizeof(*buffer->str) * (str->len))) == NULL)
    return (NULL);

  if (gen_top(str, top) == 0)
    {
      my_strncpy(buffer->str, str->str, str->len);
      buffer->len = str->len;
      return (buffer);
    }
  buffer->len = HUFFMAN_HD_SIZE;
  old_size = 0;

  my_putstr("Huffman...\n");
  i = -1;
  while (++i < str->len)
    {
      if ((index = get_topchar(top, str->str[i])) != -1)
	buffer->len = write_topchar(buffer->str, index, i == str->len - 1);
      else
	buffer->len = write_stdchar(buffer->str, str->str[i], i == str->len - 1);
      print_cent((i * 100) / str->len);
      if (buffer->len >= old_size + str->len)
	{
	  buffer->str = my_str_realloc(buffer->str, buffer->len, buffer->len + str->len);
	  if (buffer->str == NULL)
	    return (NULL);
	  old_size = buffer->len;
	}
    }
  my_putchar('\n');

  write_huffman_hd(buffer, top);
  return (buffer);
}
