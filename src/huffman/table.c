#include "my.h"
#include "cpz.h"
#include "struct.h"

int		gen_top(const t_string *str, char top[16])
{
  int		table[256];
  float		eff;
  unsigned char	tmp;
  int		set;
  int		add;
  int		i;
  int		j;
  int		k;

  gen_table(str, table);
  my_clear_str(top, 16);
  eff = 0.0;
  i = -1;
  while (++i < 16)
    {
      set = 0;
      tmp = 0;
      j = -1;
      while (++j < 256)
	if (!set || table[j] > table[tmp])
	  {
	    add = 1;
	    k = -1;
	    while (add && ++k < i)
	      add = ((unsigned char)top[k] == (unsigned char)(j)) ? 0 : add;
	    tmp = add ? j : tmp;
	    set = add ? 1 : set;
	  }
      top[i] = tmp;
      eff += (float)table[tmp] / str->len;
    }
  eff += 16.0 / str->len;
  return (eff > 0.25);
}

int	get_topchar(const char top[16], const char c)
{
  int	i;

  i = -1;
  while (++i < 16)
    if (top[i] == c)
      return (i);
  return (-1);
}
