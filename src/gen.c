#include "struct.h"

void	gen_table(const t_string *str, int table[256])
{
  int	i;

  i = -1;
  while (++i < 256)
    table[i] = 0;
  i = -1;
  while (++i < str->len)
    ++table[(unsigned char)str->str[i]];
}
