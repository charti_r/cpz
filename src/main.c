#include <stdlib.h>

#include "my.h"
#include "cpz.h"
#include "const.h"

static char	*string_option(char *option, char *arg)
{
  if (!arg)
    my_fprintf(2, "%s: [%s] option expect a character string\n", PROG_NAME, option);
  return (arg);
}

static int	run(char **argv)
{
  char		*filename;
  char		*key;
  char		option;
  int		i;

  filename = NULL;
  key = NULL;
  option = 'c';
  i = 0;
  while (argv[++i])
    {
      if (my_strcmp(argv[i], "--key") == 0)
	{
	  if ((key = string_option("--key", argv[++i])) == NULL)
	    return (-1);
	}
      else if (my_strcmp(argv[i], "--name") == 0)
	{
	  if ((filename = string_option("--name", argv[++i])) == NULL)
	    return (-1);
	}
      else if (my_strcmp(argv[i], "-c") == 0)
	option = 'c';
      else if (my_strcmp(argv[i], "-u") == 0)
	option = 'u';
      else
	{
	  if (option == 'c' && cpz(argv[i], filename, key) == -1)
	    return (-1);
	  else if (option == 'u' && uncpz(argv[i], filename, key) == -1)
	    return (-1);
	  filename = NULL;
	}
    }
  return (0);
}

int	main(int argc, char **argv)
{
  if (argc < 2 || my_strcmp(argv[1], "--help") == 0)
    {
      print_help();
      return (argc < 2 ? -1 : 0);
    }
  return (run(argv));
}
