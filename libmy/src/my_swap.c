#include "my.h"

void	my_swap_int(int *a, int *b)
{
  int	c;

  c = *a;
  *a = *b;
  *b = c;
}

void	my_swap_char(char *a, char *b)
{
  char	c;

  c = *a;
  *a = *b;
  *b = c;
}
