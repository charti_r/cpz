#include "my.h"

int	my_sizeof_char(const char *str, const char c)
{
  int	i;

  i = 0;
  while (str[i] != c && str[i])
    ++i;
  return (i);
}
