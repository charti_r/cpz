#include "my.h"

int     my_strncmp(const char *s1, const char *s2, const int n)
{
  int   i;

  i = -1;
  while (++i < n)
    if (s1[i] != s2[i])
      return (-1);
  return (0);
}
