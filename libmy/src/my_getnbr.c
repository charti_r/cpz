#include "my.h"

int	my_getnbr(const char *str)
{
  int	nb;
  int	i;

  nb = 0;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i] >= '0' && str[i] <= '9')
    {
      nb *= 10;
      nb += str[i] - '0';
      ++i;
    }
  return (nb * my_nbr_isneg(str));
}
