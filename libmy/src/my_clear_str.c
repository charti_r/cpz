#include "my.h"

char	*my_clear_str(char *str, const int size)
{
  int	i;

  i = 0;
  while (i < size)
    {
      str[i] = 0;
      ++i;
    }
  return (str);
}
