#include "my.h"

int     my_str_isupper(const char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 'A' || str[i] > 'Z')
	return (0);
      ++i;
    }
  return (1);
}
