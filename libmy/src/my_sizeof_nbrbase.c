#include "my.h"

int	my_sizeof_nbrbase(int nb, const int base)
{
  int	i;

  i = 0;
  while (nb)
    {
      nb /= base;
      ++i;
    }
  return (i);
}
