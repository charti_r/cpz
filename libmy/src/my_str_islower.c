#include "my.h"

int	my_str_islower(const char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 'a' || str[i] > 'z')
	return (0);
      ++i;
    }
  return (1);
}
