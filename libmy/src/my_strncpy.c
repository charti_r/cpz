#include "my.h"

char	*my_strncpy(char *dest, const char *src, const int n)
{
  int	i;

  i = 0;
  while (i < n)
    {
      dest[i] = src[i];
      ++i;
    }
  return (dest);
}
