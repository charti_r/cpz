#include "my.h"

void     my_putnbr_base(int nb, const char *base)
{
  int   n_base;

  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  n_base = my_strlen(base);
  if (nb >= n_base)
    {
      my_putnbr_base(nb / n_base, base);
      my_putnbr_base(nb % n_base, base);
    }
  else
    my_putchar(base[nb]);
}
