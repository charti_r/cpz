#include "my.h"

char	*my_strncat(char *dest, const char *src, const int n)
{
  int	size;
  int	i;

  size = my_strlen(dest);
  i = 0;
  while (i < n)
    {
      dest[size + i] = src[i];
      ++i;
    }
  return (dest);
}
