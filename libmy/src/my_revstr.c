#include "my.h"

char	*my_revstr(char *str)
{
  int	i;
  int	j;

  i = my_strlen(str);
  j = 0;
  while (j < i)
    my_swap_char(&str[--i], &str[j++]);
  return (str);
}
