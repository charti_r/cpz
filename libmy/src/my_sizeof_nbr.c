#include "my.h"

int	my_sizeof_nbr(int nb)
{
  int	i;

  i = 0;
  while (nb)
    {
      nb /= 10;
      ++i;
    }
  return (i);
}
