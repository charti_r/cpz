#include "my.h"

char	*my_str_realloc(char *str, const int old_size, const int new_size)
{
  char	*tmp;

  tmp = str;
  if ((str = malloc(sizeof(*str) * (new_size))) == NULL)
    return (NULL);
  my_strncpy(str, tmp, old_size);
  my_clear_str(&str[old_size], new_size - old_size);
  free(tmp);
  return (str);
}
