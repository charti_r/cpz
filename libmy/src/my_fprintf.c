#include "my.h"

static void	my_fprintf_flag(const int fd, const char flag, va_list ptr_list)
{
  if (flag == 'd' || flag == 'i')
    my_fputnbr(fd, va_arg(ptr_list, int));
  else if (flag == 'u')
    my_fputnbr(fd, va_arg(ptr_list, unsigned int));
  else if (flag == 'b')
    my_fputnbr_base(fd, va_arg(ptr_list, int), "01");
  else if (flag == 'o')
    my_fputnbr_base(fd, va_arg(ptr_list, int), "01234567");
  else if (flag == 'p' || flag == 'P')
    my_fputstr(fd, "0x");
  else if (flag == 'x' || flag == 'p')
    my_fputnbr_base(fd, va_arg(ptr_list, unsigned int), "0123456789abcdef");
  else if (flag == 'X' || flag == 'P')
    my_fputnbr_base(fd, va_arg(ptr_list, unsigned int), "0123456789ABCDEF");
  else if (flag == 'c')
    my_fputchar(fd, va_arg(ptr_list, int));
  else if (flag == 's')
    my_fputstr(fd, va_arg(ptr_list, char*));
  else if (flag == '%')
    my_fputchar(fd, '%');
}

void		my_fprintf(const int fd, const char *str, ...)
{
  va_list	ptr_list;
  int		i;

  va_start(ptr_list, str);
  i = 0;
  while (str[i])
    {
      if (str[i] == '%')
	my_fprintf_flag(fd, str[++i], ptr_list);
      else
	my_fputchar(fd, str[i]);
      ++i;
    }
  va_end(ptr_list);
}
