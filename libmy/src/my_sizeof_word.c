#include "my.h"

int	my_sizeof_word(const char *str)
{
  int	i;

  i = 0;
  while (str[i] != ' ' && str[i] != '\t' && str[i] != '\n' && str[i])
    ++i;
  return (i);
}
