#include "my.h"

int     my_str_isnum(const char *str)
{
  int   i;

  i = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i])
    {
      if (str[i] < '0' || str[i] > '9')
	return (0);
      ++i;
    }
  return (1);
}
