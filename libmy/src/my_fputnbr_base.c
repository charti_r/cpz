#include "my.h"

void     my_fputnbr_base(const int fd, int nb, const char *base)
{
  int   n_base;

  if (nb < 0)
    {
      my_fputchar(fd, '-');
      nb = -nb;
    }
  n_base = my_strlen(base);
  if (nb >= n_base)
    {
      my_fputnbr_base(fd, nb / n_base, base);
      my_fputnbr_base(fd, nb % n_base, base);
    }
  else
    my_fputchar(fd, base[nb]);
}
