#include "my.h"

char	*my_strstr(char *str, const char *to_find)
{
  int	i;
  int	j;

  i = 0;
  while (str[i])
    {
      j = 0;
      while (to_find[j] && str[i + j] && to_find[j] == str[i + j])
	++j;
      if (!to_find[j])
	return (&str[i]);
      ++i;
    }
  return ((char*)(0));
}
