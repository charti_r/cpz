static void	my_genrand(int *ptr_nb, int *ptr_seed)
{
  static int	seed = 0;

  if (ptr_seed)
    seed = *ptr_seed;
  else
    {
      *ptr_nb = ((seed * 1327217883) + 424242) & 0x7FFFFFFF;
      seed = *ptr_nb >> 1;
    }
}

void	my_srand(int seed)
{
  my_genrand((int*)(0), &seed);
}

int	my_rand(void)
{
  int	nb;

  my_genrand(&nb, (int*)(0));
  return (nb);
}
