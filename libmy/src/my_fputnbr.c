#include "my.h"

void	my_fputnbr(const int fd, int nb)
{
  if (nb < 0)
    {
      my_fputchar(fd, '-');
      nb = -nb;
    }
  if (nb >= 10)
    {
      my_fputnbr(fd, nb / 10);
      my_fputnbr(fd, nb % 10);
    }
  else
    my_fputchar(fd, nb + '0');
}
