#include "my.h"

int	my_nbr_isneg(const char *str)
{
  int	isneg;
  int	i;

  isneg = 1;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    {
      if (str[i] == '-')
	isneg = -isneg;
      ++i;
    }
  return (isneg);
}
