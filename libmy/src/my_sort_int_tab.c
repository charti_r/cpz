#include "my.h"

void	my_sort_int_tab(int *tab, const int size)
{
  int	stop;
  int	i;

  stop = 0;
  while (!stop)
    {
      stop = 1;
      i = 0;
      while ((i + 1) < size)
	{
	  if (tab[i] > tab[i + 1])
	    {
	      my_swap_int(&tab[i], &tab[i + 1]);
	      stop = 0;
	    }
	  ++i;
	}
    }
}
