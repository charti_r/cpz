#include "my.h"

int	my_charcmp(const char c, const char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] == c)
	return (1);
      ++i;
    }
  return (0);
}
