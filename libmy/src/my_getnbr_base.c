#include "my.h"

int	my_getnbr_base(const char *str, const char *base)
{
  int	n_base;
  int	nb;
  int	i;
  int	j;

  n_base = my_strlen(base);
  nb = 0;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i])
    {
      j = 0;
      while (str[i] != base[j] && base[j])
	++j;
      if (j == n_base)
	return (nb * my_nbr_isneg(str));
      nb *= n_base;
      nb += j;
      ++i;
    }
  return (nb * my_nbr_isneg(str));
}
