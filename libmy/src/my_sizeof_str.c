#include "my.h"

int	my_sizeof_str(const char *str, const char *cmp)
{
  int	i;
  int	j;

  i = 0;
  while (str[i])
    {
      j = 0;
      while (cmp[j])
	{
	  if (str[i] == cmp[j])
	    return (i);
	  ++j;
	}
      ++i;
    }
  return (i);
}
