#include "my.h"

char	*my_strcat(char *dest, const char *src)
{
  int	size;
  int	i;

  size = my_strlen(dest);
  i = 0;
  while (src[i])
    {
      dest[size + i] = src[i];
      ++i;
    }
  dest[size + i] = '\0';
  return (dest);
}
