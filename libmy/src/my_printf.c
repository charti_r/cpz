#include "my.h"

static void	my_printf_flag(const char flag, va_list list)
{
  if (flag == 'd' || flag == 'i')
    my_putnbr(va_arg(list, int));
  else if (flag == 'u')
    my_putnbr(va_arg(list, unsigned int));
  else if (flag == 'b')
    my_putnbr_base(va_arg(list, int), "01");
  else if (flag == 'o')
    my_putnbr_base(va_arg(list, int), "01234567");
  else if (flag == 'p' || flag == 'P')
    my_putstr("0x");
  else if (flag == 'x' || flag == 'p')
    my_putnbr_base(va_arg(list, unsigned int), "0123456789abcdef");
  else if (flag == 'X' || flag == 'P')
    my_putnbr_base(va_arg(list, unsigned int), "0123456789ABCDEF");
  else if (flag == 'c')
    my_putchar(va_arg(list, int));
  else if (flag == 's')
    my_putstr(va_arg(list, char*));
  else if (flag == '%')
    my_putchar('%');
}

void		my_printf(const char *str, ...)
{
  va_list	list;
  int		i;

  va_start(list, str);
  i = 0;
  while (str[i])
    {
      if (str[i] == '%')
	my_printf_flag(str[++i], list);
      else
	my_putchar(str[i]);
      ++i;
    }
  va_end(list);
}
