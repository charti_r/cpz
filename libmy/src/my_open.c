#include "my.h"

int	my_open(const char *filename, const int mode)
{
  int	fd;

  if (!(mode & O_CREAT) && access(filename, F_OK) == -1)
    {
      my_fprintf(2, "%s: no such file or directory\n", filename);
      return (-1);
    }
  if ((fd = open(filename, mode, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH)) == -1)
    my_fprintf(2, "%s: cannot open the file\n", filename);
  return (fd);
}
