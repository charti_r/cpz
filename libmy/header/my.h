#ifndef MY_H_
# define MY_H_

/*
** Std Include
*/
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <fcntl.h>

int	my_charcmp(const char c, const char *str);
char	*my_clear_str(char *str, const int size);
void	my_fprintf(const int fd, const char *str, ...);
int	my_fputchar(const int fd, const char c);
void	my_fputnbr_base(const int fd, int nb, const char *base);
void	my_fputnbr(const int fd, int nb);
int	my_fputstr(const int fd, const char *str);
int	my_getnbr_base(const char *str, const char *base);
int	my_getnbr(const char *str);
int	my_nbr_isneg(const char *str);
int	my_open(const char *filename, const int mode);
void	my_printf(const char *str, ...);
int	my_putchar(const char c);
void	my_putnbr_base(int nb, const char *base);
void	my_putnbr(int nb);
int	my_putstr(const char *str);
void	my_srand(int seed);
int	my_rand(void);
char	*my_revstr(char *str);
int	my_sizeof_char(const char *str, const char c);
int	my_sizeof_nbrbase(int nb, const int base);
int	my_sizeof_nbr(int nb);
int	my_sizeof_str(const char *str, const char *cmp);
int	my_sizeof_word(const char *str);
void	my_sort_int_tab(int *tab, const int size);
char	*my_strcapitalize(char *str);
char	*my_strcat(char *dest, const char *src);
int	my_strcmp(const char *s1, const char *s2);
char	*my_strcpy(char *dest, const char *src);
int	my_str_isalpha(const char *str);
int	my_str_islower(const char *str);
int	my_str_isnum(const char *str);
int	my_str_isprintable(const char *str);
int	my_str_isupper(const char *str);
int	my_strlen(const char *str);
char	*my_strlowcase(char *str);
char	*my_strncat(char *dest, const char *src, const int n);
int	my_strncmp(const char *s1, const char *s2, const int n);
char	*my_strncpy(char *dest, const char *src, const int n);
char	*my_str_realloc(char *str, const int old_size, const int new_size);
char	*my_strstr(char *str, const char *to_find);
char	*my_strupcase(char *str);
void	my_swap_int(int *a, int *b);
void	my_swap_char(char *a, char *b);

#endif /* !MY_H_ */
