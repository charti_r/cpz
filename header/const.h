#ifndef CONST_H_
# define CONST_H_

# define PROG_NAME	"cpz"
# define EXT		".cpz"

/*
** BUFFER_SIZE (500Mio)
*/
# define BUFFER_SIZE		(500000000)

# define LZ_HD_SIZE		6	/* 1 + 4 + 1 */
# define HUFFMAN_HD_SIZE	21	/* 1 + 4 + 16 */

# define STOP_CHAR		'0'
# define LZ_CHAR		'1'
# define HUFFMAN_CHAR		'2'
# define BTYPE_C		1
# define BTYPE_UC		0

# define LZ_DIST_MAX	0x1FF
# define LZ_LEN_MAX	0x3F

#endif /* !CONST_H_ */
