#ifndef CPZ_H_
# define CPZ_H_

# include "struct.h"

int	cpz(char *filename, char *new_filename, const char *key);
int	uncpz(char *filename, char *new_filename, const char *key);

void	gen_table(const t_string *str, int table[256]);

int	add_bit(char *file, const int value, const int last);
int	get_nextbit(const char *file, const int last);
int	put_int(char *file, const int nb);
int	get_int(const int fd);

void	print_help(void);
void	print_cent(int cent);

#endif /* !CPZ_H_ */
