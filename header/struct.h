#ifndef STRUCT_H_
# define STRUCT_H_

typedef struct	s_string
{
  char		*str;
  int		len;
}		t_string;

typedef struct	s_lz_hd
{
  int		size;
  char		esc;
}		t_lz_hd;

typedef struct	s_huffman_hd
{
  int		size;
  char		*top;
}		t_huffman_hd;

typedef struct	s_lz_field
{
  unsigned int	dist;
  unsigned char	len;
}		t_lz_field;

#endif /* !STRUCT_H_ */
