#ifndef LZ_H_
# define LZ_H_

#include "struct.h"

t_string	*cpz_lz(t_string *str);
t_string	*uncpz_lz(t_string *str);

int		find_field(const t_string *str, const int index, t_lz_field *field);

#endif /* !LZ_H_ */
