#ifndef CRYPT_H_
# define CRYPT_H_

void	crypt(t_string *str, const char *key);
void	uncrypt(t_string *str, const char *key);

#endif /* !CRYPT_H_ */
