#ifndef HUFFMAN_H_
# define HUFFMAN_H_

# include "struct.h"

t_string	*cpz_huffman(t_string *str);
t_string	*uncpz_huffman(t_string *str);

int	gen_top(const t_string *str, char top[16]);
int	get_topchar(const char top[16], const char c);

#endif /* !HUFFMAN_H_ */
